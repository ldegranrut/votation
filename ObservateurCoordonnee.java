
public class ObservateurCoordonnee implements Observateur {
    private String modification = "PAS DE MODIFICATION";

    @Override
    public void metsAJour(String attributModifie,
    Object valeur) {
        modification = "Attribut : " + attributModifie
        + " Valeur : " + valeur;
    }

    public String getModification() {
        return modification;
    }

}
